package me.tom.protohackers.problem00;

import java.io.IOException;

import me.tom.protohackers.common.tcp.ClientHandler;

public class ClientHandler00 extends ClientHandler {

  @Override
  public void run() {
    int _byte = 0;

    while (true) {
      try {
        _byte = in.read();
        if (_byte == -1) {
          break;
        }

        out.write(_byte);
        out.flush();
      } catch (IOException e) {
        break;
      }
    }

    close();
  }
}

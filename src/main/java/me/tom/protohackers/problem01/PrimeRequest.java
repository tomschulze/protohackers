package me.tom.protohackers.problem01;

import java.util.stream.IntStream;

public record PrimeRequest(String method, Number number) {

  public PrimeRequest(String method, Number number) {
    this.method = method;
    this.number = number;
    validate();
  }

  public void validate() {
    boolean isValidMethod = method.equals("isPrime");
    if (!isValidMethod) {
      throw new MalformedRequestException("Invalid method");
    }

    if (number == null) {
      throw new MalformedRequestException("Invalid number");
    }
  }

  private boolean isInteger(Number number) {
    return number instanceof Integer;
  }

  // https://www.baeldung.com/java-prime-numbers
  public boolean isPrime() {
    if (!isInteger(number)) {
      return false;
    }

    int nr = number.intValue();

    return nr > 1
        && IntStream.rangeClosed(2, (int) Math.sqrt(nr))
            .noneMatch(n -> (nr % n == 0));
  }
}

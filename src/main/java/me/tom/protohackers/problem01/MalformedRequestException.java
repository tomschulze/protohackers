package me.tom.protohackers.problem01;

public class MalformedRequestException extends RuntimeException {
  public MalformedRequestException(String message) {
    super(message);
  }
}

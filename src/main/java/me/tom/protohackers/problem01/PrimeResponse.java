package me.tom.protohackers.problem01;

public record PrimeResponse(String method, boolean prime) {
  public static PrimeResponse from(PrimeRequest request) {
    return new PrimeResponse("isPrime", request.isPrime());
  }

  public static PrimeResponse newInvalid() {
    return new PrimeResponse("error", false);
  }
}

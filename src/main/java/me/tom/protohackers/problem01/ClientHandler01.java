package me.tom.protohackers.problem01;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.cfg.CoercionAction;
import com.fasterxml.jackson.databind.cfg.CoercionInputShape;
import com.fasterxml.jackson.databind.type.LogicalType;

import me.tom.protohackers.common.tcp.ClientHandler;

public class ClientHandler01 extends ClientHandler {

  @Override
  public void run() {
    int _byte = 0;
    // TODO consider using a ByteBuffer instead to avoid copying bytes
    // the message size is predictable
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    ObjectMapper mapper = getObjectMapper();

    while (true) {
      try {
        _byte = in.read();
      } catch (IOException e) {
        break;
      }

      if (_byte == -1) {
        break;
      }

      if (_byte == 10) {
        PrimeResponse response;
        boolean isMalformedRequest = false;

        byte[] requestBytes = buffer.toByteArray();
        try {
          PrimeRequest request = mapper.readValue(requestBytes, PrimeRequest.class);
          response = PrimeResponse.from(request);
        } catch (JsonMappingException e) {
          response = PrimeResponse.newInvalid();
          isMalformedRequest = true;
        } catch (IOException e) {
          response = PrimeResponse.newInvalid();
          isMalformedRequest = true;
        }

        byte[] responseBytes = getInvalidResponseBytes();
        try {
          responseBytes = mapper.writeValueAsBytes(response);
        } catch (JsonProcessingException e) {
          Logger.getGlobal().info(
              "JsonProcessingException while writing bytes from response object. Defaulting to invalid response.");
        }

        try {
          writeAndFlush(responseBytes);
          if (isMalformedRequest) {
            // disconnect the client and clean up...
            break;
          }
        } catch (IOException e) {
          // client disconnectected remotely? clean up...
          break;
        }

        buffer.reset();
        continue;
      }

      buffer.write(_byte);
    }

    close();
  }

  private void writeAndFlush(byte[] bytes) throws IOException {
    out.write(bytes);
    out.write(10);
    out.flush();
  }

  private byte[] getInvalidResponseBytes() {
    return "{\"method\":\"error\", \"prime\": false}".getBytes();
  }

  private ObjectMapper getObjectMapper() {
    ObjectMapper mapper = new ObjectMapper();

    // do not coerce string numbers to int
    mapper
        .coercionConfigFor(LogicalType.Integer)
        .setCoercion(CoercionInputShape.String, CoercionAction.Fail);

    // ignore extra fields/properties
    mapper
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    return mapper;
  }
}

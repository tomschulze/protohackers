package me.tom.protohackers;

import me.tom.protohackers.common.ProtoServer;
import me.tom.protohackers.common.tcp.ProtoServerTCP;
import me.tom.protohackers.common.tcp.SocketRepository;
import me.tom.protohackers.common.udp.ProtoServerUDP;

public class ProtoServerFactory {
  String problemNr;
  ClientHandlerFactory clientHandlerFactory;
  UDPHandlerFactory udpHandlerFactory;
  SocketRepository socketRepository;

  public ProtoServerFactory(String problemNr) {
    this.problemNr = problemNr;
    this.socketRepository = new SocketRepository();
    this.clientHandlerFactory = new ClientHandlerFactory(problemNr, socketRepository);
    this.udpHandlerFactory = new UDPHandlerFactory(problemNr);
  }

  public ProtoServer create() {
    switch (problemNr) {
      case "00":
        return new ProtoServerTCP(clientHandlerFactory);
      case "01":
        return new ProtoServerTCP(clientHandlerFactory);
      case "02":
        return new ProtoServerTCP(clientHandlerFactory);
      case "03":
        return new ProtoServerTCP(clientHandlerFactory);
      case "04":
        return new ProtoServerUDP(udpHandlerFactory);
      case "05":
        return new ProtoServerTCP(clientHandlerFactory);

      default:
        throw new UnsupportedOperationException(
            "ClientHandler for problem number '" + problemNr + "' is not implemented yet");
    }
  }
}

package me.tom.protohackers.problem05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class MobClient {
  private String host;
  private int port;

  private Socket clientSocket;
  private OutputStream out;
  private BufferedReader in;

  public MobClient(String host, int port) {
    this.host = host;
    this.port = port;
  }

  public void startConnection() throws UnknownHostException, IOException {
    clientSocket = new Socket(host, port);
    out = clientSocket.getOutputStream();
    in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
  }

  public byte[] readAndForgeMessage() throws IOException {
    String message = in.readLine();
    String forgedMessage = ChatMessage.forgeFrom(message).getMessage();
    forgedMessage = forgedMessage + "\n";

    return forgedMessage.getBytes();
  }

  public void forgeAndSendMessage(byte[] message) throws IOException {
    String forgedMessage = ChatMessage.forgeFrom(message)
        .getMessage();
    out.write(forgedMessage.getBytes());
    out.write(10);
    out.flush();
  }

  public void stopConnection() throws IOException {
    if (clientSocket == null) {
      return;
    }

    clientSocket.close();
  }

  public boolean isClosed() {
    return clientSocket.isClosed();
  }
}

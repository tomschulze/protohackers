package me.tom.protohackers.problem05;

import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChatMessage {

  private final String message;
  private static final String BOGUSCOIN_TONY = "7YWHMfk9JZe0LM0g1ZauHuiSxhI";
  private static final Pattern PATTERN = Pattern
      .compile("\\b7[A-Za-z0-9]{25,34}\\b");

  public ChatMessage(byte[] buffer) {
    this.message = new String(buffer, 0, buffer.length);
  }

  public ChatMessage(String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }

  public ChatMessage forge() {
    String forgedMessage = new String(message);

    List<String> addresses = findAllBoguscoinAddresses();
    for (String address : addresses) {
      forgedMessage = forgedMessage.replace(address, BOGUSCOIN_TONY);
    }

    return new ChatMessage(forgedMessage);
  }

  // this one finds all boguscoin substrings and then
  // makes sure they are at the beginning or end of message
  // and are followed or preceded by space only
  // HACK is there a more elegant way to match?
  // NOTE message must not contain the final newline in order for this to work
  private List<String> findAllBoguscoinAddresses() {
    Matcher matcher = PATTERN.matcher(message);

    List<String> res = matcher
        .results()
        .filter(match -> isValidBoguscoinAddress(match, message))
        .map(match -> message.substring(match.start(), match.end()))
        .toList();

    return res;
  }

  public static ChatMessage forgeFrom(String message) {
    return new ChatMessage(message).forge();
  }

  public static ChatMessage forgeFrom(byte[] message) {
    return new ChatMessage(message).forge();
  }

  private static boolean isValidBoguscoinAddress(MatchResult match, String message) {
    int leftNeighbor = match.start() - 1;
    int rightNeighbor = match.end();

    boolean leftOK = leftNeighbor == -1 || message.charAt(leftNeighbor) == ' ';
    boolean rightOK = rightNeighbor == message.length() || message.charAt(rightNeighbor) == ' ';

    return leftOK && rightOK;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((message == null) ? 0 : message.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ChatMessage other = (ChatMessage) obj;
    if (message == null) {
      if (other.message != null)
        return false;
    } else if (!message.equals(other.message))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return this.message;
  }
}

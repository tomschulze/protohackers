package me.tom.protohackers.problem05;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Logger;

import me.tom.protohackers.common.tcp.ClientHandler;

public class ClientHandler05 extends ClientHandler {
  private MobClient mobClient;
  private static final String UPSTREAM_HOST = "chat.protohackers.com";
  private static final int UPSTREAM_PORT = 16963;

  @Override
  public void run() {
    this.mobClient = new MobClient(UPSTREAM_HOST, UPSTREAM_PORT);
    int _byte = 0;
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();

    try {
      mobClient.startConnection();
    } catch (IOException e) {
      Logger.getGlobal().info("Cannot connect to upstream server");
      close();
      return;
    }

    Thread thread = new Thread(new MobHandler(this, mobClient));
    thread.start();

    while (true) {
      try {
        _byte = in.read();
      } catch (IOException e) {
        break;
      }

      if (_byte == -1) {
        break;
      }

      if (_byte == 10) {
        try {
          mobClient.forgeAndSendMessage(buffer.toByteArray());
        } catch (IOException e) {
          Logger.getGlobal().info("Cannot write to upstream server");
          break;
        }

        buffer.reset();
        continue;
      }

      buffer.write(_byte);
    }

    close();
  }

  public void writeAndFlush(byte[] bytes) throws IOException {
    out.write(bytes);
    out.flush();
  }

  @Override
  protected void close() {
    try {
      mobClient.stopConnection();
      in.close();
      out.close();
      clientSocket.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}

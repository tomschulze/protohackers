package me.tom.protohackers.problem05;

import java.io.IOException;

class MobHandler implements Runnable {
  private ClientHandler05 client;
  private MobClient mobClient;

  public MobHandler(ClientHandler05 client, MobClient mobClient) {
    this.client = client;
    this.mobClient = mobClient;
  }

  @Override
  public void run() {
    while (true) {
      if (mobClient.isClosed()) {
        break;
      }

      try {
        byte[] line = mobClient.readAndForgeMessage();
        client.writeAndFlush(line);
      } catch (IOException e) {
        break;
      }
    }
  }
}
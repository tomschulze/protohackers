package me.tom.protohackers;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

import me.tom.protohackers.problem04.KenKeyValueService;
import me.tom.protohackers.problem04.KenKeyValueStore;
import me.tom.protohackers.problem04.UDPHandler04;

public class UDPHandlerFactory {
  private String problemNr;
  private KenKeyValueStore store;
  private KenKeyValueService service;

  public UDPHandlerFactory(String problemNr) {
    this.problemNr = problemNr;
    this.store = new KenKeyValueStore();
    this.service = new KenKeyValueService(store);
  }

  // missing cases are tcp problems
  public UDPHandler04 create(DatagramSocket socket, DatagramPacket packet) throws UnsupportedOperationException {
    switch (problemNr) {
      case "04":
        return new UDPHandler04(socket, packet, service);

      default:
        throw new UnsupportedOperationException(
            "UDPHandler for problem number '" + problemNr + "' is not implemented yet");
    }
  }
}

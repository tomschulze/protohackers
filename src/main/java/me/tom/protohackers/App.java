package me.tom.protohackers;

import java.io.IOException;
import java.net.SocketException;

import me.tom.protohackers.common.ProtoServer;

public class App {
  public static Integer PORT = 4321;

  public static void main(String[] args) throws Exception {
    String problemNr = new String("00");

    if (args != null && args.length > 0) {
      problemNr = new String(args[0]);
    }

    ProtoServerFactory serverFactory = new ProtoServerFactory(problemNr);
    ProtoServer server = serverFactory.create();

    try {
      server.listen(PORT);
    } catch (SocketException e) {
      throw e;
    } catch (IOException e) {
      throw new RuntimeException("Cannot start server for problem: '" + problemNr +
          "'");
    } catch (UnsupportedOperationException e) {
      throw e;
    }
  }
}

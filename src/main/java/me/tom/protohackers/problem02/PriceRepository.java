package me.tom.protohackers.problem02;

import java.util.ArrayList;
import java.util.OptionalDouble;
import java.util.stream.Stream;

public class PriceRepository {
  private ArrayList<Price> prices;

  public PriceRepository() {
    prices = new ArrayList<>();
  }

  public void insert(Price price) {
    prices.add(price);
  }

  public int count() {
    return prices.size();
  }

  public int queryAverage(int minTime, int maxTime) {
    if (minTime > maxTime) {
      return 0;
    }

    Stream<Price> stream = prices.stream();
    OptionalDouble average = stream
        .filter((price) -> {
          boolean isSmaller = price.timestamp() <= maxTime;
          boolean isGreater = price.timestamp() >= minTime;
          return isSmaller && isGreater;
        })
        .mapToInt(price -> price.price())
        .average();

    return (int) average.orElse(0);
  }
}

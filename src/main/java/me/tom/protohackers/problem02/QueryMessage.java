package me.tom.protohackers.problem02;

public record QueryMessage(int int1, int int2) implements Message {
}

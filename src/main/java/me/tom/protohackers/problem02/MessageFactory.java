package me.tom.protohackers.problem02;

import java.nio.ByteBuffer;

public class MessageFactory {
  private final static int MESSAGE_LENGTH = 9;

  public static Message create(byte[] bytes) throws RuntimeException {
    if (bytes.length != MESSAGE_LENGTH) {
      throw new RuntimeException("Message length does not match.");
    }

    ByteBuffer buffer = ByteBuffer.wrap(bytes);
    char type = (char) buffer.get(0);
    int int1 = buffer.slice(1, 4).getInt();
    int int2 = buffer.slice(5, 4).getInt();

    if (type == 'I') {
      return new InsertMessage(int1, int2);
    }

    if (type == 'Q') {
      return new QueryMessage(int1, int2);
    }

    throw new RuntimeException("Invalid message type specified");
  }
}

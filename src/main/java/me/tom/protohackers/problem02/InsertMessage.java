package me.tom.protohackers.problem02;

public record InsertMessage(int int1, int int2) implements Message {
}

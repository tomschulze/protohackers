package me.tom.protohackers.problem02;

import java.util.Optional;

public class PriceService {
  private PriceRepository repository;

  public PriceService(PriceRepository repository) {
    this.repository = repository;
  }

  public Optional<Integer> processMessage(Message message) {
    int int1 = message.int1();
    int int2 = message.int2();

    if (message instanceof InsertMessage) {
      insert(int1, int2);
      return Optional.empty();
    }

    if (message instanceof QueryMessage) {
      return Optional.of(queryAverage(int1, int2));
    }

    throw new UnsupportedOperationException("Message type not implemented");
  }

  private void insert(int int1, int int2) {
    Price price = new Price(int1, int2);
    repository.insert(price);
  }

  private int queryAverage(int int1, int int2) {
    if (int1 > int2) {
      return 0;
    }

    return repository.queryAverage(int1, int2);
  }
}

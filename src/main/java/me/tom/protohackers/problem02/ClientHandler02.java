package me.tom.protohackers.problem02;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Optional;

import me.tom.protohackers.common.tcp.ClientHandler;

public class ClientHandler02 extends ClientHandler {
  @Override
  public void run() {
    int _byte = 0;
    // TODO consider using a ByteBuffer instead to avoid copying bytes
    // the message size is predictable
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    PriceRepository repository = new PriceRepository();
    PriceService service = new PriceService(repository);

    while (true) {
      try {
        _byte = in.read();
      } catch (IOException e) {
        break;
      }

      if (_byte == -1) {
        break;
      }

      buffer.write(_byte);

      if (buffer.size() == 9) {
        byte[] bytes = buffer.toByteArray();
        buffer.reset();

        Optional<Integer> result = Optional.empty();
        try {
          Message message = MessageFactory.create(bytes);
          result = service.processMessage(message);
        } catch (RuntimeException e) {
          // invalid message specification
          continue;
        }

        if (result.isPresent()) {
          try {
            byte[] resultBytes = bytesFromInteger(result.get());
            writeAndFlush(resultBytes);
          } catch (IOException e) {
            // client disconnectected remotely? clean up...
            break;
          }
        }

        continue;
      }
    }

    close();
  }

  private void writeAndFlush(byte[] bytes) throws IOException {
    out.write(bytes);
    out.flush();
  }

  private byte[] bytesFromInteger(int i) {
    ByteBuffer buffer = ByteBuffer.allocate(4);
    buffer.putInt(i);
    return buffer.array();
  }
}

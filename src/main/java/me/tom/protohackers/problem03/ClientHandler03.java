package me.tom.protohackers.problem03;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import me.tom.protohackers.common.tcp.ClientHandler;

public class ClientHandler03 extends ClientHandler {
  private ChatService chatService;

  public ClientHandler03(ChatService chatService) {
    this.chatService = chatService;
  }

  private boolean hasConnected = false;
  private ChatName chatName;

  @Override
  public void run() {
    int _byte = 0;
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();

    try {
      writeAndFlush("Hello, your Name?".getBytes());
    } catch (IOException e) {
      disconnectAndClose();
      return;
    }

    while (true) {
      try {
        _byte = in.read();
      } catch (IOException e) {
        break;
      }

      if (_byte == -1) {
        break;
      }

      if (_byte == 10) {
        byte[] requestBytes = buffer.toByteArray();

        if (hasConnected()) {
          broadcast(requestBytes);
        } else {
          try {
            this.chatName = new ChatName(requestBytes);
            connect();
          } catch (InvalidChatNameException e) {
            break;
          } catch (IOException e) {
            break;
          }
        }

        buffer.reset();
        continue;
      }

      buffer.write(_byte);
    }

    disconnectAndClose();
    return;
  }

  private boolean hasConnected() {
    return this.hasConnected;
  }

  private void connect() throws IOException {
    chatService.connect(clientSocket, chatName);
    this.hasConnected = true;
    byte[] whois = chatService.whoIsInTheRoom(clientSocket);
    if (whois.length > 0) {
      writeAndFlush(whois);
    }
  }

  private void disconnectAndClose() {
    chatService.disconnect(clientSocket);
    close();
  }

  private void broadcast(byte[] message) {
    chatService.broadcast(clientSocket, message);
  }

  private void writeAndFlush(byte[] bytes) throws IOException {
    out.write(bytes);
    out.write(10);
    out.flush();
  }
}

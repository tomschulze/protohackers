package me.tom.protohackers.problem03;

import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import me.tom.protohackers.common.tcp.SocketRepository;

public final class ChatService {
  private SocketRepository socketRepository;
  private HashMap<String, ChatName> chatNames;

  public ChatService(SocketRepository socketRepository) {
    this.socketRepository = socketRepository;
    this.chatNames = new HashMap<>();
  }

  public void broadcast(Socket sender, byte[] message) {
    broadcastRaw(sender, message, true);
  }

  private void broadcastRaw(Socket sender, byte[] message, boolean useChatName) {
    String chatName = getChatName(sender);
    String chatPrefix = "[" + chatName + "] ";

    this.socketRepository.getClients()
        .forEach((receiver) -> {
          if (receiver.equals(sender))
            return;

          try {
            if (useChatName) {
              receiver.getOutputStream().write(chatPrefix.getBytes());
            }
            receiver.getOutputStream().write(message);
            receiver.getOutputStream().write(10);
            receiver.getOutputStream().flush();
          } catch (IOException e) {
            Logger.getGlobal().info("Client is not reachable at " + getRemoteAddress(receiver));
          }
        });
  }

  public byte[] whoIsInTheRoom(Socket sender) {
    String message = new String("* The room contains: ");

    String chatNames = socketRepository.getClients()
        .filter((client) -> !client.equals(sender))
        .map(client -> getChatName(client))
        .collect(Collectors.joining(", "));

    return (message + chatNames).getBytes();
  }

  public void connect(Socket clientSocket, ChatName chatName) {
    String remoteAddress = getRemoteAddress(clientSocket);

    this.socketRepository.add(clientSocket);
    this.chatNames.putIfAbsent(remoteAddress, chatName);

    String name = chatName.toString();
    String message = new String("* " + name + " has entered the room");
    broadcastRaw(clientSocket, message.getBytes(), false);
  }

  public void disconnect(Socket clientSocket) {
    String chatName = getChatName(clientSocket);
    String remoteAddress = getRemoteAddress(clientSocket);

    this.socketRepository.remove(clientSocket);
    this.chatNames.remove(remoteAddress);

    if (!chatName.isEmpty()) {
      String message = new String("* " + chatName + " has left the room");
      broadcastRaw(clientSocket, message.getBytes(), false);
    }
  }

  private String getChatName(Socket client) {
    String remoteAddress = getRemoteAddress(client);
    ChatName chatName = this.chatNames.get(remoteAddress);
    if (chatName == null) {
      return "";
    }

    return chatName.toString();
  }

  private String getRemoteAddress(Socket client) {
    int port = client.getPort();
    String ip = client.getInetAddress().getHostAddress();

    return ip + ":" + port;
  }
}

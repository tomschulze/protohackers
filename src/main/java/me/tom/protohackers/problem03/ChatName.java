package me.tom.protohackers.problem03;

import java.nio.charset.Charset;

public record ChatName(byte[] name) {
  public ChatName(byte[] name) {
    this.name = name;
    validate();
  }

  private void validate() throws InvalidChatNameException {
    if (name.length > 20) {
      throw new InvalidChatNameException("Name must not be longer than 20 characters.");
    }

    boolean result = false;
    for (byte _byte : name) {
      result = isAlphanumeric(_byte);
    }

    if (!result) {
      throw new InvalidChatNameException("Chat name must conist of alphanumeric characters only");
    }
  }

  private boolean isAlphanumeric(int codePoint) {
    return (codePoint >= 65 && codePoint <= 90) ||
        (codePoint >= 97 && codePoint <= 122) ||
        (codePoint >= 48 && codePoint <= 57);
  }

  @Override
  public final String toString() {
    return new String(name, Charset.defaultCharset());
  }
}

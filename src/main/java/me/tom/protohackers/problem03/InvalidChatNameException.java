package me.tom.protohackers.problem03;

public class InvalidChatNameException extends RuntimeException {
  public InvalidChatNameException(String message) {
    super(message);
  }
}

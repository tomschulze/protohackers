package me.tom.protohackers.common;

import java.io.IOException;

public interface ProtoServer {
  public void listen(int port) throws IOException;
}

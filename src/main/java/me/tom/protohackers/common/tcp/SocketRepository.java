package me.tom.protohackers.common.tcp;

import java.net.Socket;
import java.util.ArrayList;
import java.util.stream.Stream;

// TODO is locking or a similar approach necessary? tests passed w/o locks
public class SocketRepository {
  private ArrayList<Socket> clients;

  public SocketRepository() {
    this.clients = new ArrayList<>();
  }

  public void add(Socket clientSocket) {
    this.clients.add(clientSocket);
  }

  public void remove(Socket clientSocket) {
    this.clients.remove(clientSocket);
  }

  public Stream<Socket> getClients() {
    return this.clients.stream();
  }
}

package me.tom.protohackers.common.tcp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public abstract class ClientHandler implements Runnable {
  protected Socket clientSocket;
  protected OutputStream out;
  protected InputStream in;

  protected void bindSocket(Socket socket) throws IOException {
    this.clientSocket = socket;
    out = this.clientSocket.getOutputStream();
    in = this.clientSocket.getInputStream();
  }

  protected void close() {
    try {
      in.close();
      out.close();
      clientSocket.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}

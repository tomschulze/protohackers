package me.tom.protohackers.common.tcp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.logging.Logger;

import me.tom.protohackers.ClientHandlerFactory;
import me.tom.protohackers.common.ProtoServer;

public class ProtoServerTCP implements ProtoServer {
  private ServerSocket serverSocket;
  private ClientHandlerFactory factory;

  public ProtoServerTCP(ClientHandlerFactory factory) {
    this.factory = factory;
  }

  public void listen(int port) throws IOException, UnsupportedOperationException {
    serverSocket = new ServerSocket(port);

    while (true) {
      Socket clientSocket = null;
      ClientHandler clientHandler = factory.create();

      try {
        clientSocket = serverSocket.accept();
      } catch (SocketException e) {
        Logger.getGlobal().info("Server Socket was closed");
        break;
      }

      clientHandler.bindSocket(clientSocket);
      Thread clientThread = new Thread(clientHandler);
      clientThread.start();

      SocketAddress address = clientSocket.getRemoteSocketAddress();

      Logger.getGlobal().info("New client connected from " + address);
    }
  }

  public void close() {
    try {
      serverSocket.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
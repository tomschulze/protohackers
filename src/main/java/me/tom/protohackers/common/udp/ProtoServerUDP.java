package me.tom.protohackers.common.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.logging.Logger;

import me.tom.protohackers.UDPHandlerFactory;
import me.tom.protohackers.common.ProtoServer;

public class ProtoServerUDP implements ProtoServer {
  private DatagramSocket serverSocket;
  private UDPHandlerFactory factory;

  public ProtoServerUDP(UDPHandlerFactory factory) {
    this.factory = factory;
  }

  public void listen(int port) throws IOException, SocketException {

    try {
      serverSocket = new DatagramSocket(port);
    } catch (SocketException e) {
      throw e;
    }

    while (true) {
      byte[] buffer = new byte[999];
      DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
      serverSocket.receive(packet);
      Logger.getGlobal().info("Packet received from " + packet.getSocketAddress());
      UDPHandler udpHandler = factory.create(serverSocket, packet);
      udpHandler.run();
    }
  }
}

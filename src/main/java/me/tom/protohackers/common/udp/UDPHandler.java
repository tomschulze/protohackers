package me.tom.protohackers.common.udp;

public interface UDPHandler extends Runnable {
  public void run();
}

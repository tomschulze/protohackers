package me.tom.protohackers;

import me.tom.protohackers.common.tcp.ClientHandler;
import me.tom.protohackers.common.tcp.SocketRepository;
import me.tom.protohackers.problem00.ClientHandler00;
import me.tom.protohackers.problem01.ClientHandler01;
import me.tom.protohackers.problem02.ClientHandler02;
import me.tom.protohackers.problem03.ChatService;
import me.tom.protohackers.problem03.ClientHandler03;
import me.tom.protohackers.problem05.ClientHandler05;

public class ClientHandlerFactory {
  private String problemNr;
  private ChatService chatService;

  public ClientHandlerFactory(String problemNr, SocketRepository socketRepository) {
    this.problemNr = problemNr;
    this.chatService = new ChatService(socketRepository);
  }

  // case "04" is a UDP problem
  public ClientHandler create() throws UnsupportedOperationException {
    switch (problemNr) {
      case "00":
        return new ClientHandler00();
      case "01":
        return new ClientHandler01();
      case "02":
        return new ClientHandler02();
      case "03":
        return new ClientHandler03(chatService);
      case "05":
        return new ClientHandler05();

      default:
        throw new UnsupportedOperationException(
            "ClientHandler for problem number '" + problemNr + "' is not implemented yet");
    }
  }
}

package me.tom.protohackers.problem04;

import java.util.HashMap;

public class KenKeyValueStore {
  private final HashMap<String, String> keyValueStore = new HashMap<>();
  private final String RESERVED_KEY_VERSION = "version";
  private final String VERSION = "Ken's Key-Value Store 1.0";

  public void insert(String key, String value) {
    if (key.equals(RESERVED_KEY_VERSION)) {
      return;
    }

    keyValueStore.put(key, value);
  }

  public String retrieve(String key) {
    if (key.equals(RESERVED_KEY_VERSION)) {
      return VERSION;
    }

    return keyValueStore.get(key);
  }

}

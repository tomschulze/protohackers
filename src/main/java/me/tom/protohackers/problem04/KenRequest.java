package me.tom.protohackers.problem04;

import java.net.DatagramPacket;

public class KenRequest {
  private int equalMarker;
  private byte[] data;
  private int packetLength;

  public KenRequest(DatagramPacket packet) {
    this.data = packet.getData();
    this.packetLength = packet.getLength();
    setEqualMarker();
  }

  private void setEqualMarker() {
    for (int i = 0; i < data.length; i++) {
      if (data[i] == 61) {
        this.equalMarker = i;
        return;
      }
    }

    this.equalMarker = -1;
  }

  public boolean isInsertRequest() {
    return equalMarker >= 0;
  }

  public String getValue() {
    if (!isInsertRequest()) {
      return "";
    }

    int offset = equalMarker + 1;
    int bytesToDecode = packetLength - equalMarker - 1;

    // request is a single equal sign: "="
    if (equalMarker == 0 && packetLength == 1) {
      return "";
    }

    return new String(data, offset, bytesToDecode);
  }

  public String getKey() {
    if (isInsertRequest()) {
      return new String(data, 0, equalMarker);
    }

    return new String(data, 0, packetLength);
  }
}

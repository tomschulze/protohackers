package me.tom.protohackers.problem04;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Optional;
import java.util.logging.Logger;

import me.tom.protohackers.common.udp.UDPHandler;

public class UDPHandler04 implements UDPHandler {
  private DatagramSocket socket;
  private KenKeyValueService service;

  private int destPort;
  private InetAddress destAddress;
  private DatagramPacket packet;

  public UDPHandler04(DatagramSocket socket, DatagramPacket packet, KenKeyValueService service) {
    this.socket = socket;
    this.service = service;
    this.packet = packet;

    destAddress = packet.getAddress();
    destPort = packet.getPort();
  }

  public void run() {
    KenRequest request = new KenRequest(packet);
    Optional<byte[]> response = service.processRequest(request);

    if (response.isEmpty()) {
      return;
    }

    byte[] responseData = response.get();
    DatagramPacket packetToClient = new DatagramPacket(responseData, responseData.length);
    try {
      sendPacket(packetToClient);
    } catch (IOException e) {
      Logger.getGlobal().info("Cannot send packet. " + e.getMessage());
    }
  }

  private void sendPacket(DatagramPacket packet) throws IOException {
    packet.setAddress(destAddress);
    packet.setPort(destPort);
    socket.send(packet);
  }
}

package me.tom.protohackers.problem04;

import java.util.Optional;

public class KenKeyValueService {
  KenKeyValueStore store;

  public KenKeyValueService(KenKeyValueStore store) {
    this.store = store;
  }

  public Optional<byte[]> processRequest(KenRequest request) {
    if (request.isInsertRequest()) {
      insert(request);
      return Optional.empty();
    }

    return retrieve(request);
  }

  private void insert(KenRequest request) {
    String key = request.getKey();
    String value = request.getValue();

    store.insert(key, value);
  }

  private Optional<byte[]> retrieve(KenRequest request) {
    String key = request.getKey();
    String value = store.retrieve(key);

    if (value == null) {
      return Optional.empty();
    }

    String response = key + "=" + value;
    return Optional.of(response.getBytes());
  }
}

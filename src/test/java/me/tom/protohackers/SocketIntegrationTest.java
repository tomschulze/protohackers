package me.tom.protohackers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import me.tom.protohackers.common.tcp.ProtoServerTCP;
import me.tom.protohackers.common.tcp.SocketRepository;

@TestInstance(value = TestInstance.Lifecycle.PER_CLASS)
public class SocketIntegrationTest {
  ProtoServerTCP server;
  ExecutorService executorService;
  private static int port;

  public SocketIntegrationTest() throws IOException, InterruptedException {
    // Take an available port
    ServerSocket s = new ServerSocket(0);
    port = s.getLocalPort();
    s.close();

    SocketRepository repository = new SocketRepository();
    ClientHandlerFactory factory = new ClientHandlerFactory("00", repository);
    server = new ProtoServerTCP(factory);

    executorService = Executors.newSingleThreadExecutor();
    executorService.submit(() -> {
      try {
        server.listen(port);
      } catch (IOException e) {
        e.printStackTrace();
      }
    });
  }

  @AfterAll
  // TODO check test lifecycle
  // sometimes when a test fails in another test suite
  // this test fails, because server is already closed
  public void tearDown() throws IOException, InterruptedException {
    server.close();
    executorService.shutdown();

    try {
      if (!executorService.awaitTermination(20, TimeUnit.SECONDS)) {
        executorService.shutdownNow();
      }
    } catch (InterruptedException ex) {
      executorService.shutdownNow();
      Thread.currentThread().interrupt();
    }
  }

  @Test
  public void testSingleClient() throws UnknownHostException, IOException {
    TestClient client = new TestClient();
    client.startConnection("127.0.0.1", port);
    String response = client.sendMessage("hello server");
    assertEquals("hello server", response);
    client.stopConnection();
  }

  @Test
  public void testMultiClient() throws UnknownHostException, IOException {
    TestClient client1 = new TestClient();
    TestClient client2 = new TestClient();
    TestClient client3 = new TestClient();
    TestClient client4 = new TestClient();

    client1.startConnection("127.0.0.1", port);
    client2.startConnection("127.0.0.1", port);
    client3.startConnection("127.0.0.1", port);
    client4.startConnection("127.0.0.1", port);

    String response1 = client1.sendMessage("hello1");
    String response2 = client2.sendMessage("hello2");
    String response3 = client1.sendMessage("hello3");
    String response4 = client2.sendMessage("hello4");

    client1.stopConnection();
    client2.stopConnection();
    client3.stopConnection();
    client4.stopConnection();

    assertEquals("hello1", response1);
    assertEquals("hello2", response2);
    assertEquals("hello3", response3);
    assertEquals("hello4", response4);
  }
}

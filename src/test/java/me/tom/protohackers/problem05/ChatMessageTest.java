package me.tom.protohackers.problem05;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class ChatMessageTest {
  @ParameterizedTest
  @MethodSource("provideChatMessageParameters")
  public void testChatMessage(String message, ChatMessage expectedMessage) {
    ChatMessage chatMessage = new ChatMessage(message);
    ChatMessage forgedMessage = chatMessage.forge();
    assertEquals(expectedMessage, forgedMessage);
  }

  private static Stream<Arguments> provideChatMessageParameters() {
    String tonysAddress = "7YWHMfk9JZe0LM0g1ZauHuiSxhI";

    String address1 = "7F1u3wSD5RbOHQmupo9nx4TnhQ";
    String address2 = "7iKDZEwPZSqIvDnHvVN2r0hUWXD5rHX";
    String address3 = "7adNeSwJkMakpEcln9HEtthSRtxdmEHOT8T";
    String invalidAddress1 = "7F1u3wSD5RbOHQmupo9nx4Tnh";
    String invalidAddress2 = "7adNeSwJkMakpEcln9HEtthSRtxdmEHOT8Tx";

    // messages w/ valid boguscoin addresses
    String msg1 = "Please send the payment of 750 Boguscoins to $$";
    String msg2 = "$$ is where you send the money.";
    String msg3 = "Send money here: $$ or here: $$";

    // messages w/ invalid boguscoin addresses
    String msg4 = "Address is followed by a dot. Payment here: $$.";
    String msg5 = "This is a product ID: $$-JZTmp04Wq0Ev5BHP6vV0iKnWPwRI-1234";
    String msg6 = "This is another ID with '#': $$#JZTmp04Wq0Ev5BHP6vV0iKnWPwRI-1234";
    String msg7 = "This is a normal chat message w/o boguscoin addresses.";

    return Stream.of(
        // strings composed of boguscoin addresses only
        Arguments.of(address1, expectedChatMessage("$$", tonysAddress)),
        Arguments.of(address2, expectedChatMessage("$$", tonysAddress)),
        Arguments.of(address3, expectedChatMessage("$$", tonysAddress)),
        Arguments.of(invalidAddress1, expectedChatMessage("$$", invalidAddress1)),
        Arguments.of(invalidAddress2, expectedChatMessage("$$", invalidAddress2)),

        // messages to be forged
        Arguments.of(insertBoguscoin(msg1, address1), expectedChatMessage(msg1, tonysAddress)),
        Arguments.of(insertBoguscoin(msg2, address1), expectedChatMessage(msg2, tonysAddress)),
        Arguments.of(insertBoguscoin(msg3, address1), expectedChatMessage(msg3, tonysAddress)),

        // messages not to be forged
        Arguments.of(insertBoguscoin(msg4, address1), expectedChatMessage(msg4, address1)),
        Arguments.of(insertBoguscoin(msg5, address1), expectedChatMessage(msg5, address1)),
        Arguments.of(insertBoguscoin(msg6, address1), expectedChatMessage(msg6, address1)),
        Arguments.of(msg7, expectedChatMessage(msg7, "")),
        Arguments.of(insertBoguscoin(msg1, invalidAddress1), expectedChatMessage(msg1, invalidAddress1)));
  }

  public static String insertBoguscoin(String message, String boguscoin) {
    return message.replace("$$", boguscoin);
  }

  public static ChatMessage expectedChatMessage(String message, String boguscoin) {
    return new ChatMessage(insertBoguscoin(message, boguscoin));
  }
}

package me.tom.protohackers.problem02;

import java.nio.ByteBuffer;

public class ByteUtil {
  public static byte[] createMessage(char type, int number1, int number2) {
    byte messageType = (byte) type;
    byte[] int1 = fromInteger(number1);
    byte[] int2 = fromInteger(number2);

    return ByteBuffer
        .allocate(9)
        .put(messageType)
        .put(int1)
        .put(int2)
        .array();
  }

  public static byte[] fromInteger(int value) {
    ByteBuffer buffer = ByteBuffer.allocate(4);

    return buffer.putInt(value).array();
  }
}

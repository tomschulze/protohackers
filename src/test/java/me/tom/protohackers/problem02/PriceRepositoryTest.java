package me.tom.protohackers.problem02;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

public class PriceRepositoryTest {
  @Test
  public void testInsertDifferentTimestampPrices() {
    PriceRepository repository = new PriceRepository();
    List<Price> prices = List.of(
        new Price(12345455, 233),
        new Price(12454555, 233),
        new Price(123555455, -233));

    prices.forEach((price) -> repository.insert(price));

    assertEquals(3, repository.count());
  }

  @Test
  public void testInsertInsertTwoEqualTimestampPrices() {
    PriceRepository repository = new PriceRepository();
    List<Price> prices = List.of(
        new Price(12345455, 222),
        new Price(12345455, 233),
        new Price(12345435, 2000));

    prices.forEach((price) -> repository.insert(price));
    assertEquals(3, repository.count());
  }

  @Test
  public void testValidQueryAveragePrice() {
    PriceRepository repository = new PriceRepository();
    Price price = new Price(12345455, 200);
    Price price2 = new Price(12454555, 10);
    Price price3 = new Price(123555455, 23);
    repository.insert(price);
    repository.insert(price2);
    repository.insert(price3);

    int averagePrice = repository.queryAverage(0, 123555455);

    assertEquals(77, averagePrice);
  }

  @Test
  public void testInvalidQueryAveragePrice() {
    PriceRepository repository = new PriceRepository();
    Price price = new Price(12345455, 100);
    Price price2 = new Price(12454555, 100);
    Price price3 = new Price(123555455, 25);
    repository.insert(price);
    repository.insert(price2);
    repository.insert(price3);

    // min is bigger than max -> return 0
    int averagePrice1 = repository.queryAverage(12454555, 12345455);

    // no value exists within interval -> return 0
    int averagePrice2 = repository.queryAverage(11345455, 12345454);

    assertEquals(0, averagePrice1);
    assertEquals(0, averagePrice2);
  }
}

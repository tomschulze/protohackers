package me.tom.protohackers.problem02;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

public class MeansEndIntegrationTest {

  @Test
  public void testProcessValidInsertMessages() {
    List<byte[]> insertOnly = List.of(
        ByteUtil.createMessage('I', 1710520035, 1000),
        ByteUtil.createMessage('I', 1710510035, 1000),
        ByteUtil.createMessage('I', 1710500035, -1000));

    PriceRepository repository = new PriceRepository();
    PriceService service = new PriceService(repository);
    insertOnly.forEach((bytes) -> {
      Message message = MessageFactory.create(bytes);
      Optional<Integer> result = service.processMessage(message);
      assertTrue(result.isEmpty());
    });

    assertEquals(3, repository.count());
  }

  @Test
  public void testProcessValidQueryMessages() {
    List<byte[]> insertOnly = List.of(
        ByteUtil.createMessage('Q', 0, 1710520035),
        ByteUtil.createMessage('Q', 0, 0),
        ByteUtil.createMessage('Q', -1, 0));

    PriceRepository repository = new PriceRepository();
    PriceService service = new PriceService(repository);
    insertOnly.forEach((bytes) -> {
      Message message = MessageFactory.create(bytes);
      Optional<Integer> result = service.processMessage(message);
      assertTrue(result.isPresent());
      assertEquals(0, result.get());
    });

    assertEquals(0, repository.count());
  }

  @Test
  public void testProcessInsertAndQueryMessages() {
    List<byte[]> inserts = List.of(
        ByteUtil.createMessage('I', 1710520034, 1000),
        ByteUtil.createMessage('I', -1710510035, 1000));

    PriceRepository repository = new PriceRepository();
    PriceService service = new PriceService(repository);
    inserts.forEach((bytes) -> {
      Message message = MessageFactory.create(bytes);
      service.processMessage(message);
    });

    byte[] queryBytes = ByteUtil.createMessage('Q', -1710510035, 1710510035);
    Message queryMessage = MessageFactory.create(queryBytes);
    Optional<Integer> result = service.processMessage(queryMessage);
    assertEquals(2, repository.count());
    assertEquals(1000, result.get());
  }
}

package me.tom.protohackers.problem02;

import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class MessageTest {

  @Test
  public void testGenerateValidMessageFromBytes() {
    byte[] insertBytes = ByteUtil.createMessage('I', 1710520035, 10);
    byte[] queryBytes = ByteUtil.createMessage('Q', 1710510035, 1710520035);

    Message message1 = MessageFactory.create(insertBytes);
    Message message2 = MessageFactory.create(queryBytes);
    assertInstanceOf(InsertMessage.class, message1);
    assertInstanceOf(QueryMessage.class, message2);
  }

  @Test
  public void testInvalidMessageFromBytes() {
    byte[] invalidMessageType = ByteUtil.createMessage('Z', 1710520035, 10);
    byte[] invalidByteCount = new byte[2];
    assertThrows(RuntimeException.class, () -> MessageFactory.create(invalidMessageType));
    assertThrows(RuntimeException.class, () -> MessageFactory.create(invalidByteCount));
  }
}

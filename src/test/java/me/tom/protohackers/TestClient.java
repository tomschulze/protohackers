package me.tom.protohackers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class TestClient {
  private Socket clientSocket;
  private DataOutputStream out;
  private DataInputStream in;

  public void startConnection(String ip, int port) throws UnknownHostException, IOException {
    clientSocket = new Socket(ip, port);
    out = new DataOutputStream(clientSocket.getOutputStream());
    in = new DataInputStream(clientSocket.getInputStream());
  }

  public String sendMessage(String msg) throws IOException {
    out.writeUTF(msg);
    out.flush();
    String resp = in.readUTF();
    return resp;
  }

  public void stopConnection() throws IOException {
    in.close();
    out.close();
    clientSocket.close();
  }
}

package me.tom.protohackers.problem01;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.io.IOException;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.cfg.CoercionAction;
import com.fasterxml.jackson.databind.cfg.CoercionInputShape;
import com.fasterxml.jackson.databind.type.LogicalType;

public class PrimeTimeIntegrationTest {

  @Test
  public void testGenerateValidResponseFromRequest() throws IOException {
    PrimeRequest request = new PrimeRequest("isPrime", 5);
    PrimeResponse response = PrimeResponse.from(request);

    assertEquals(true, response.prime());
    assertEquals("isPrime", response.method());
  }

  @ParameterizedTest
  @MethodSource("provideInvalidRequestParameters")
  public void testGenerateInvalidResponseFromInvalidRequest(String message, Number number) throws IOException {
    PrimeResponse response = new PrimeResponse("isPrime", true);

    try {
      new PrimeRequest(message, number);
    } catch (MalformedRequestException e) {
      response = PrimeResponse.newInvalid();
    }

    assertNotEquals("isPrime", response.method());
  }

  private static Stream<Arguments> provideInvalidRequestParameters() {
    return Stream.of(
        // invalid method name
        Arguments.of("invalidMethod", 2),
        // valid method name, invalid number
        Arguments.of("isPrime", null));
  }

  @ParameterizedTest
  @MethodSource("provideValidJsonParameters")
  public void testGenerateValidResponseFromJson(String json, PrimeRequest request, boolean isPrime)
      throws JsonMappingException, JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    PrimeRequest convertedRequest = mapper.readValue(json, PrimeRequest.class);

    assertEquals(request, convertedRequest);
    assertEquals(isPrime, PrimeResponse.from(convertedRequest).prime());
  }

  private static Stream<Arguments> provideValidJsonParameters() {
    String jsonClean = "{\"method\": \"isPrime\",\"number\": 2}";
    String jsonExtra = "{\"method\": \"isPrime\",\"number\": 2, \"extra\": 2}";
    String jsonFloat = "{\"method\": \"isPrime\",\"number\": 2.5}";

    PrimeRequest request = new PrimeRequest("isPrime", 2);
    PrimeRequest requestFloat = new PrimeRequest("isPrime", 2.5);

    return Stream.of(
        Arguments.of(jsonClean, request, true),
        Arguments.of(jsonExtra, request, true),
        Arguments.of(jsonFloat, requestFloat, false));
  }

  @ParameterizedTest
  @MethodSource("provideInvalidJsonParameters")
  public void testGenerateInvalidResponseFromInvalidJson(String json) throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    mapper.coercionConfigFor(LogicalType.Integer)
        .setCoercion(CoercionInputShape.String, CoercionAction.Fail);

    PrimeResponse response = null;

    try {
      mapper.readValue(json, PrimeRequest.class);
    } catch (JsonMappingException e) {
      response = PrimeResponse.newInvalid();
    }

    assertEquals("error", response.method());
  }

  private static Stream<Arguments> provideInvalidJsonParameters() {
    return Stream.of(
        // non parsable json
        Arguments.of("\"method\": \"isPrime\", \"number\": \"2\"}"),
        // invalid number
        Arguments.of("{\"method\": \"isPrime\", \"number\": \"2\"}"),
        Arguments.of("{\"method\": \"isPrime\", \"number\": \"a\"}"),
        Arguments.of("{\"method\": \"isPrime\", \"number\": true}"),
        // missing field 'number'
        Arguments.of("{\"method\": \"isPrime\"}"),
        // missing field 'method'
        Arguments.of("{\"number\": 2}"));
  }

  @Test
  public void testGenerateValidRequestFromBytes() throws IOException, JsonMappingException, JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    String json = "{\"method\": \"isPrime\",\"number\": 2}";
    byte[] bytes = json.getBytes();

    PrimeRequest request = new PrimeRequest("isPrime", 2);
    PrimeRequest convertedRequest = mapper.readValue(bytes, PrimeRequest.class);

    assertEquals(request, convertedRequest);
  }

  @Test
  public void testGenerateValidBytesFromResponse() throws IOException, JsonMappingException, JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    PrimeResponse response = new PrimeResponse("isPrime", true);
    String validJson = "{\"method\":\"isPrime\",\"prime\":true}";
    String json = mapper.writeValueAsString(response);
    byte[] bytes = mapper.writeValueAsBytes(response);

    assertEquals(validJson, json);
    assertArrayEquals(validJson.getBytes(), bytes);
  }
}

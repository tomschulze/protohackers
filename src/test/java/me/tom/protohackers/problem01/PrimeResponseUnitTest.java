package me.tom.protohackers.problem01;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

public class PrimeResponseUnitTest {
  @Test
  public void testValidResponse() {
    PrimeResponse response = new PrimeResponse("isPrime", true);
    assertEquals("isPrime", response.method());
    assertEquals(true, response.prime());
  }

  @Test
  public void testInvalidResponse() {
    PrimeResponse response = new PrimeResponse("error", true);
    assertNotEquals("isPrime", response.method());
  }
}

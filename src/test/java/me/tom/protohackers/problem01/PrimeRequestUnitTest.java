package me.tom.protohackers.problem01;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;

import org.junit.jupiter.api.Test;

public class PrimeRequestUnitTest {

  @Test
  public void testPrimeRequestValidates() {
    assertThrows(RuntimeException.class, () -> new PrimeRequest("wrongMethod", 2), "Invalid method");
    assertThrows(RuntimeException.class, () -> new PrimeRequest("primeNumber", null), "Invalid number");
  }

  @Test
  public void testPrimeRequestIsPrime() throws IOException {
    PrimeRequest request = new PrimeRequest("isPrime", 7);
    assertEquals(true, request.isPrime());
  }

  @Test
  public void testPrimeRequestIsNotPrimeForInteger() throws IOException {
    PrimeRequest request = new PrimeRequest("isPrime", 8);
    assertEquals(false, request.isPrime());
  }

  @Test
  public void testPrimeRequestIsNotPrimeForFloat() throws IOException {
    PrimeRequest request = new PrimeRequest("isPrime", 13.3);
    assertEquals(false, request.isPrime());
  }
}

package me.tom.protohackers.problem03;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class ChatNameTest {
  @ParameterizedTest
  @MethodSource("provideValidNames")
  public void testValidChatName(String readableName, byte[] name) {
    ChatName chatName = new ChatName(name);

    assertEquals(readableName, chatName.toString());
    assertDoesNotThrow(() -> new ChatName(name));
  }

  private static Stream<Arguments> provideValidNames() {
    String name1 = "MyNameIsValid";
    String name2 = "122my Name Is Valid";
    String name3 = "122";
    String name4 = "1";
    String name5 = "a";

    return Stream.of(
        Arguments.of(name1, name1.getBytes()),
        Arguments.of(name2, name2.getBytes()),
        Arguments.of(name3, name3.getBytes()),
        Arguments.of(name4, name4.getBytes()),
        Arguments.of(name5, name5.getBytes()));
  }

  @ParameterizedTest
  @MethodSource("provideInvalidNames")
  public void testInvalidChatName(byte[] name) {
    assertThrows(InvalidChatNameException.class, () -> new ChatName(name));
  }

  private static Stream<Arguments> provideInvalidNames() {
    return Stream.of(Arguments.of(
        "MyNameIsWaaaayyToooLong".getBytes(),
        // non alphanumeric characters
        "DDD|||".getBytes()));
  }
}

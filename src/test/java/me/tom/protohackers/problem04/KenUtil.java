package me.tom.protohackers.problem04;

import java.net.DatagramPacket;

public class KenUtil {
  public static DatagramPacket createDatagramPacket(String message) {
    byte[] bytes = message.getBytes();
    return new DatagramPacket(bytes, bytes.length);
  }

  public static KenRequest createKenRequest(String message) {
    DatagramPacket packet = createDatagramPacket(message);
    return new KenRequest(packet);
  }
}

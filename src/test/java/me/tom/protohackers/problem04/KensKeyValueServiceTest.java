package me.tom.protohackers.problem04;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class KensKeyValueServiceTest {

  private KenKeyValueService service;
  private KenKeyValueStore store;

  @BeforeEach
  public void init() {
    store = new KenKeyValueStore();
    service = new KenKeyValueService(store);
  }

  @ParameterizedTest
  @MethodSource("provideValidInserts")
  public void testInsert(KenRequest request) {
    Optional<byte[]> response = service.processRequest(request);
    assertTrue(response.isEmpty());
  }

  private static Stream<Arguments> provideValidInserts() {
    return Stream.of(
        Arguments.of(KenUtil.createKenRequest("bar=foo")),
        Arguments.of(KenUtil.createKenRequest("=foo")),
        Arguments.of(KenUtil.createKenRequest("bar=")),
        Arguments.of(KenUtil.createKenRequest("bar\n=")),
        Arguments.of(KenUtil.createKenRequest("bar=\n")),
        Arguments.of(KenUtil.createKenRequest("=")));
  }

  @ParameterizedTest
  @MethodSource("provideValidRequests")
  public void testRetrieve(KenRequest request, byte[] expected) {

    service.processRequest(KenUtil.createKenRequest("key=val"));
    service.processRequest(KenUtil.createKenRequest("key=updatedVal"));
    service.processRequest(KenUtil.createKenRequest("noVal="));
    service.processRequest(KenUtil.createKenRequest("keySpace =foo"));
    service.processRequest(KenUtil.createKenRequest("valSpaces=foo   "));
    service.processRequest(KenUtil.createKenRequest("valNewline=fooNew\n"));
    service.processRequest(KenUtil.createKenRequest("keyNewline\n=foo"));
    service.processRequest(KenUtil.createKenRequest("="));

    Optional<byte[]> response = service.processRequest(request);
    assertTrue(response.isPresent());
    assertArrayEquals(expected, response.get());
  }

  private static Stream<Arguments> provideValidRequests() {
    return Stream.of(
        Arguments.of(KenUtil.createKenRequest("key"), "key=updatedVal".getBytes()),
        Arguments.of(KenUtil.createKenRequest("noVal"), "noVal=".getBytes()),
        Arguments.of(KenUtil.createKenRequest("keySpace "), "keySpace =foo".getBytes()),
        Arguments.of(KenUtil.createKenRequest("valSpaces"), "valSpaces=foo   ".getBytes()),
        Arguments.of(KenUtil.createKenRequest("valNewline"), "valNewline=fooNew\n".getBytes()),
        Arguments.of(KenUtil.createKenRequest("keyNewline\n"), "keyNewline\n=foo".getBytes()),
        Arguments.of(KenUtil.createKenRequest(""), "=".getBytes()));
  }
}

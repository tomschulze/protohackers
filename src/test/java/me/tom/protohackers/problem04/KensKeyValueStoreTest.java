package me.tom.protohackers.problem04;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class KensKeyValueStoreTest {

  KenKeyValueStore store;

  @BeforeEach
  public void initKeyValueStore() {
    store = new KenKeyValueStore();
  }

  @ParameterizedTest
  @MethodSource("provideValidValues")
  public void tesInsertAndRetrieveValidValues(String key, String value) {
    assertDoesNotThrow(() -> store.insert(key, value));

    String actual = store.retrieve(key);
    assertEquals(value, actual);
  }

  private static Stream<Arguments> provideValidValues() {
    return Stream.of(
        Arguments.of("key", "value"),
        Arguments.of("", "value"),
        Arguments.of("key", ""),
        Arguments.of("", ""));
  }

  @Test
  public void testInsertingVersionIsIgnored() {
    String version = "Ken's Key-Value Store 1.0";

    store.insert("version", "my overwritten version");
    assertEquals(version, store.retrieve("version"));
  }
}

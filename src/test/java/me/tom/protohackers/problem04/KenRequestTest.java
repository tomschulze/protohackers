package me.tom.protohackers.problem04;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class KenRequestTest {
  @ParameterizedTest
  @MethodSource("provideValidRequests")
  public void testInsertRequest(KenRequest request, boolean isInsert, String key, String value) {
    assertEquals(isInsert, request.isInsertRequest());
    assertEquals(key, request.getKey());
    assertEquals(value, request.getValue());
  }

  private static Stream<Arguments> provideValidRequests() {
    return Stream.of(
        Arguments.of(KenUtil.createKenRequest("bar=foo"), true, "bar", "foo"),
        Arguments.of(KenUtil.createKenRequest("bar =foo"), true, "bar ", "foo"),
        Arguments.of(KenUtil.createKenRequest("bar=foo "), true, "bar", "foo "),
        Arguments.of(KenUtil.createKenRequest("=foo"), true, "", "foo"),
        Arguments.of(KenUtil.createKenRequest("=foo  "), true, "", "foo  "),
        Arguments.of(KenUtil.createKenRequest("bar="), true, "bar", ""),
        Arguments.of(KenUtil.createKenRequest("= "), true, "", " "),
        Arguments.of(KenUtil.createKenRequest("="), true, "", ""),
        Arguments.of(KenUtil.createKenRequest("bar"), false, "bar", ""),
        Arguments.of(KenUtil.createKenRequest("bar "), false, "bar ", ""),
        Arguments.of(KenUtil.createKenRequest(""), false, "", ""));
  }
}

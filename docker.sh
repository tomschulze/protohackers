#!/bin/sh

version=$1
if [ -z $version ]; then
  echo "usage: ./docker.sh <version>"
  echo "for example ./docker.sh 0.0.1 => protohackers:0.0.1"
  exit 0
fi

docker build -t protohackers:$version .

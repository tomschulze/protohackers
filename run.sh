#!/bin/sh

problemNr=$1
if [ -z $problemNr ]; then
  echo "usage: ./run.sh <problemNr>"
  echo "for example ./run.sh 01"
  exit 0
fi

mvn exec:java -Dexec.arguments=$problemNr

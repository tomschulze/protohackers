#!/bin/sh

ip=$1
if [ -z $ip ]; then
  echo "usage: ./deploy.sh <remote ip>"
  echo "for example ./deploy.sh 127.0.0.1"
  exit 0
fi

filename=protohackers.tar

echo "building image..." && \
./docker.sh 1.0.0 1>/dev/null && \
echo "saving image to file $PWD/$filename"
docker save -o $filename protohackers:1.0.0 && \
echo "sending image file to $ip:/root/$filename" && \
scp $filename root@$ip:/root/ && \
echo "deleting $filename" && \
rm -rf $filename

echo "done"

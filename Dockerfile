FROM maven:3.9.6-eclipse-temurin-21-alpine AS build
RUN mkdir /project
COPY . /project
WORKDIR /project
RUN mvn clean package

FROM eclipse-temurin:21.0.2_13-jre-alpine
RUN mkdir /app
RUN addgroup --system javauser && adduser -S -s /bin/false -G javauser javauser
COPY --from=build project/target/protohackers-1.0.0-SNAPSHOT.jar /app/protohackers-1.0.0-SNAPSHOT.jar
WORKDIR /app
RUN chown -R javauser:javauser /app
USER javauser

EXPOSE 4321

ENTRYPOINT ["java", "-jar", "protohackers-1.0.0-SNAPSHOT.jar"]
CMD ["00"]
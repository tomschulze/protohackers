# Protohackers solutions

Java solutions for the protohackers TCP challenges. See https://protohackers.com for problems.

Run `mvn exec:java -Dexec.arguments=<problemNr>` where `<problemNr>` is a zero-padded number from 00 to the latest problem number available (if implemented). Or alternatively run `./run.sh <problemNr>`.

Run `mvn test` to run ... tests.

Run `docker.sh <version>` to build a docker image.

Start the docker container w/ `docker run --rm -p 4321:4321 protohackers:<version> <problemNr>`. If `<problemNr>` is omitted, it defaults to 00. Use `-p 4321:4321/udp` if you are expecting to receive UDP packets.


## VSCode 

```json
// launch.json
{
  "version": "0.2.0",
  "configurations": [
    {
      "type": "java",
      "name": "App",
      "request": "launch",
      "mainClass": "me.tom.protohackers.App",
      "projectName": "protohackers",
      "args": "${command:SpecifyProgramArgs}"
    }
  ]
}
```

```json
// settings.json
{
  "java.configuration.updateBuildConfiguration": "disabled",
  "[java]": {
    "editor.tabSize": 2,
    "editor.insertSpaces": true,
    "editor.formatOnSave": true
  },
}
```